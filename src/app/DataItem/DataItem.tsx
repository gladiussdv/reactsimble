import React from 'react';
import styles from './DataItem.module.scss';
import {DataModel} from "../services/data.model";
import {Link} from "react-router-dom";
import {RouteConst} from "../route.const";
import { data } from '../services/data';

const DataItem = ({item, index}: {item: DataModel, index: number}) => {

    const onDelete = (index: number) => {
        data.removeDataItem(index);
    }

    return <div className={styles.DataItem} data-testid="DataItem">
        <span>{item.title}</span>
        <span>{item.description}</span>
        <Link to={RouteConst.editData + '/' + index}>Edit</Link>
        <button onClick={(event) => onDelete(index)}>Delete</button>
    </div>
};

export default DataItem;
