import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DataItem from './DataItem';

describe('<DataItem />', () => {
  test('it should mount', () => {
    render(<DataItem  />);
    
    const dataItem = screen.getByTestId('DataItem');

    expect(dataItem).toBeInTheDocument();
  });
});