import {DataModel} from "./data.model";
import {Observable, of, Subject, Subscriber} from "rxjs";
import {tap} from "rxjs/operators";

const dataList: DataModel[] = [
    {
        id: 0,
        title: 'hello',
        description: 'world'
    }
];
export class Data {
    subscriber: Subscriber<DataModel[]> | undefined;
    observable: Observable<DataModel[]> = new Observable<DataModel[]>(subscriber1 => {
        debugger;
        this.subscriber = subscriber1;
        this.subscriber.next(dataList);
    });

    constructor() {}

    getDataList(): Observable<DataModel[]> {
        debugger;
        return this.observable.pipe(tap(item => {
            debugger;
        }));
    }

    updateDataItem(index: number, dataModel: DataModel): Observable<void> {
        dataList[index] = {...dataModel};
        this.subscriber?.next(dataList);
        return of();
    }

    removeDataItem(index: number): Observable<void> {
        dataList.splice(index, 1);
        this.subscriber?.next(dataList);
        return of();
    }

    getDataItem(index: number): Observable<DataModel> {
        return of(dataList[index]);
    }

    addDataItem(dataModel: DataModel): Observable<number> {
        dataList.push({...dataModel});
        this.subscriber?.next(dataList);
        return of(dataList.length - 1);
    }
}

export const data = new Data();