import {DataModel} from "../services/data.model";

import {createAsyncThunk, createReducer} from "@reduxjs/toolkit";
import {increment, loadedList} from "./actions";

export interface State {
    dataList: DataModel[];
    increment: number;
}

export const initialState: State = {
    dataList: [],
    increment: 0
}

// export const dataReducer = createReducer(initialState, builder => {
//     builder.addCase(loadedList, (state, aciton) => {
//         return {...state, dataList: [...aciton.payload]}
//     });
//     builder.addCase(increment, (state, action) => {
//         return {...state, increment: state.increment++};
//     })
// });

// function dataApp(state: State, aciton) {
//     if (typeof state === 'undefined') {
//         return initialState;
//     }
//     return state;
// }


export const dataReducer = createReducer(initialState, {
    [loadedList.type]: (state, action) => {
        return {...state, dataList: [...action.payload]};
    },
    // [fetchData.fulfilled.type]: (state, action) => {
    //     debugger;
    //     return {...state, dataList: [...action.payload]};
    // },
    [increment.type]: (state, action) => {
        return {...state, increment: state.increment + 1};
    }
});