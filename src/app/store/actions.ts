import {DataModel} from "../services/data.model";
import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {data} from "../services/data";

// export const dataActions = {
//     loadList: 'LOAD_LIST',
//     loadedList: 'LOADED_LIST',
//
//     increment: 'INCREMENT'
// }

export const loadList = createAction<void>('Load_List');
// () => ({type: dataActions.loadList});
export const loadedList = createAction<DataModel[]>('Loaded_List');

export const increment = createAction<void>('Increment');
// (list: DataModel[]) => ({type: dataActions.loadedList, list});
