import {configureStore} from "@reduxjs/toolkit";
import {dataReducer, initialState, State} from "./reducers";
import {ActionsObservable, combineEpics, createEpicMiddleware, Epic, ofType} from "redux-observable";
import {Action} from "rxjs/internal/scheduler/Action";
import {filter, map, mapTo, switchMap, tap} from "rxjs/operators";
import {loadedList, loadList} from "./actions";
import {Observable} from "rxjs";
import {createStoreHook} from "react-redux";
import {isOfType} from "typesafe-actions";
import {data} from "../services/data";

const epicMiddleWare = createEpicMiddleware<any, any, State>();


export const store = configureStore({
    reducer: dataReducer,
    preloadedState: initialState,
    // @ts-ignore
    middleware: [epicMiddleWare]
});

export type AppDispatch = typeof store.dispatch;

export const loadData$ = (action$: Observable<any>) => {
    return action$.pipe(
        filter(isOfType(loadList.type)),
        switchMap(() => {
            return data.getDataList();
        }),
        tap((list) => {
            store.dispatch(loadedList(list));
        }),
        mapTo({type: ''})
    );
}

epicMiddleWare.run(combineEpics(loadData$));
