import {State} from "./reducers";

export const dataListSelector = (state: State) => state?.dataList;