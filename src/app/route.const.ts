export enum RouteConst {
    home = '/',
    about = '/about',
    dashboard = '/dashboard',
    editData = '/editData'
}