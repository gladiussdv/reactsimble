import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import EditData from './EditData';

describe('<EditData />', () => {
  test('it should mount', () => {
    render(<EditData />);
    
    const editData = screen.getByTestId('EditData');

    expect(editData).toBeInTheDocument();
  });
});