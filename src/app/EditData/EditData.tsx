import React, {SyntheticEvent, useEffect, useState} from 'react';
import styles from './EditData.module.scss';
import {DataModel} from "../services/data.model";
import {Formik} from "formik";
import { useParams, useHistory } from 'react-router-dom';
import {data} from "../services/data";
import {RouteConst} from "../route.const";
import DataItem from "../DataItem/DataItem";
import {of, Subscription} from "rxjs";
import {first} from "rxjs/operators";

const EditData = () => {
    let subscription: Subscription = new Subscription();
    let history = useHistory();
    const params: {index: string} = useParams();
    const index = params.index != null && !isNaN(Number(params.index)) ? parseInt(params.index, 10): undefined;

    const [item, setItem] = useState({
        id: 0,
        title: '',
        description: ''
    });
    const handleSubmit = (evt: SyntheticEvent) => {
        debugger;
    }

    useEffect(() => {
        subscription.add((index != null ? data.getDataItem(index) : of(item)).subscribe(dataModel => {
            setItem({...dataModel});
        }));
        return () => subscription.unsubscribe();
    }, []);

    return <div className={styles.EditData} data-testid="EditData">
        <Formik
            enableReinitialize={true}
            initialValues={item}
            validate={values => {
                const errors: {title: undefined | string, description: undefined | string}
                    = {title: undefined, description: undefined};
                if (!values.title) {
                    errors.title = 'Required';
                }
                if (!values.description) {
                    errors.description = 'Required'
                }
            }
            }
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    debugger;
                    // alert(JSON.stringify(values, null, 2));
                    if (index != null) {
                        subscription.add(data.updateDataItem(index, values).pipe(first()).subscribe(() => {
                            console.log('updated success!');
                        }));
                    } else {
                        subscription.add(data.addDataItem(values).pipe(first()).subscribe((newIndex) => {
                            history.push(`${RouteConst.editData}/${newIndex}`);
                        }));
                    }
                    setSubmitting(false);
                }, 400);
            }}
        >
            {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
              }) => (
                <form onSubmit={handleSubmit}>
                    <input
                        type="text"
                        name="title"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.title}
                    />
                    {errors.title && touched.title && errors.title}
                    <input
                        type="text"
                        name="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                    />
                    {errors.description && touched.description && errors.description}
                    <button type="submit" disabled={isSubmitting}>
                        Submit
                    </button>
                </form>
            )}
        </Formik>
    </div>
};

export default EditData;
