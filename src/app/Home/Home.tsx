import React, {useEffect, useState} from 'react';
import styles from './Home.module.scss';
import {data} from "../services/data";
import DataItem from "../DataItem/DataItem";
import {RouteConst} from "../route.const";
import {Link} from "react-router-dom";
import {AppDispatch} from "../store";
import {increment, loadList} from "../store/actions";
import {useDispatch, useSelector} from "react-redux";
import {State} from "../store/reducers";

const getSelectorFunc = (state: State) => state.dataList;

export const Home = () => {
    const [dataList, setDataList] = useState();
    const dispatch: AppDispatch = useDispatch();
    const dataItemList = useSelector(getSelectorFunc);
    // const incrementValue = useSelector((state: State) => state.increment);



    useEffect(() => {
        console.log('home loaded');
        dispatch(loadList());
        dispatch(increment());

        // store.getState().dataList;


        // const subscription = data.getDataList().subscribe(itemList => {
        //     setDataList(itemList
        //         .map((item, index) => <DataItem item={item} index={index} key={index}></DataItem>));
        // });
        // return () => subscription.unsubscribe();
    }, []);

    return <div className={styles.Home} data-testid="Home">
        <div>
            Home Component
        </div>
        <div>
            <Link to={RouteConst.editData}>Add</Link>
        </div>
        {dataItemList.map((item, index) => <DataItem item={item} index={index} key={index}></DataItem>)}
        {/*{incrementValue}*/}
    </div>
};
