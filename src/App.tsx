import React from 'react';
import logo from './logo.svg';
import './App.scss';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import About from "./app/About/About";
import Dashboard from './app/Dashboard/Dashboard';
import EditData from './app/EditData/EditData';
import DataItem from "./app/DataItem/DataItem";
import {RouteConst} from "./app/route.const";
import { Home } from './app/Home/Home';
import {Provider} from "react-redux";
import {store} from "./app/store";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <ul>
            <li>
              <Link to={RouteConst.home}>Home</Link>
            </li>
            <li>
              <Link to={RouteConst.about}>About</Link>
            </li>
            <li>
              <Link to={RouteConst.dashboard}>Dashboard</Link>
            </li>
          </ul>

          <hr />
          <Switch>
            <Route exact path={RouteConst.home}>
              <Home />
            </Route>
            <Route path={RouteConst.about}>
              <About />
            </Route>
            <Route path={RouteConst.dashboard}>
              <Dashboard />
            </Route>
            {/*<Route exect path={RouteConst.editData} component={EditData}/>*/}
            <Route exect path={RouteConst.editData + '/:index?'} component={EditData}/>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
